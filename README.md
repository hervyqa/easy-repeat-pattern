# About

This template will help you to create seamless pattern easily. Auto generate mockup preview. Default size 3000pt(4000px).

- Version     : 2.1
- License     : GNU GPLv3

## Creator
- Name        : Hervy Qurrotul Ainur Rozi
- Telegram/IG : [@hervyqa](https://t.me/hervyqa)
- Mastodon    : [@hervyqa@mastodon.art](https://mastodon.art/@hervyqa)
- Donate      : [paypal.me/hervyqa](https://paypal.me/hervyqa)

## Community FLOSS Pattern
- Telegram/IG : [@muslimpattern](https://t.me/muslimpattern)
- Support by  : [@vectara.id](https://instagram/vectara.id) (VectAra Creativepreneur Indonesia)

# Download Template

[Template v2.1](https://gitlab.com/hervyqa/easy-repeat-pattern/uploads/54194ed7d5206c8dbdc5e9802d270950/easy-repeat-pattern-v2.1.zip)

# Demonstration

[![Inkscape SVG Template - Easy Repeat Pattern + Mockup](https://img.youtube.com/vi/i0j-v9tOjV4/0.jpg)](https://www.youtube.com/watch?v=i0j-v9tOjV4 "Inkscape SVG Template - Easy Repeat Pattern + Mockup")

# Tutorial (EN):

- copy directory "templates", paste to :
    + linux   : `~/.config/inkscape/`
    + windows : `C:\Users\<Username>\Appdata\Roaming\Inkscape\`
- Default size 96DPI: 3000x3000pt = 4000x4000px.
- File "Easy Repeat Pattern.svg" used for a simple repeat pattern without mockup. (lighweight).
- File "Easy Repeat Pattern Mockup.svg" used for pattern with full mockup. (need more cpu).
- Open inkscape, open from new templates (ctrl+shift+n). choose "Easy Repeat Pattern" or "Easy Repeat Pattern + Mockup".
- Please use Object Dialog to object management. Open in Menu > Object > Object.
- Make sure your pattern elements to the "your-design-here" group.
- If you want export to PNG you can export with exporter inkscape by default, or use inkporter extension. please use id "pattern" to export pattern texture. id "mockup-1x1" to export pattern type 1x1. id "mockup-3x3" to export mockup tpe 3x3. download inkporter from https://inkscape.org/~raniaamina/%E2%93%85inkporter.
- If you want save the pattern 3x3 to EPS, move the canvas first by the click of the "pattern-3x3" group. Then press "Ctrl+Shift+R". After that it may be saved as EPS file.
- Other template & extensions: https://inkscape.org/~hervyqa/

# Tutorial (ID):

- Salin (Copy) direktori "templates", lalu tempel (paste) ke :
    + linux   : `~/.config/inkscape/`
    + windows : `C:\Users\<Username>\Appdata\Roaming\Inkscape\`
- Ukuran bawaan 96DPI: 3000x3000pt = 4000x4000px.
- Berkas "Easy Repeat Pattern.svg" digunakan untuk pattern berulang sederhana tanpa mockup. (ringan).
- Berkas "Easy Repeat Pattern Mockup.svg" digunakan untuk pattern berulang dengan full mockup. (membutuhkan daya cpu).
- Buka inkscape, buka dari template baru (ctrl+alt+n). pilih "Easy Repeat Pattern" atau "Easy Repeat Pattern + Mockup".
- Gunakan Objek dialog untuk manajemen objek. Buka di Menu > Object > Object.
- Pastikan elemen pattern anda berada di grup "your-design-here".
- Jika ingin mengekspor ke PNG bisa menggunakan espor bawaan inkscape, atau menggunakan ekstensi inkporter. gunakan id "pattern" untuk mengekspor textur pattern. id "mockup-1x1" untuk ekspor mockup 1x1. dan id "mockup-3x3" untuk mengekspor mockup 3x3. unduh inkporter dari https://inkscape.org/~raniaamina/%E2%93%85inkporter.
- Jika Anda menginginkan untuk menyimpan pattern tipe 3x3 ke berkas EPS, pindahkan kanvas terlebih dahulu dengan mengklik grup "pattern-3x3". Lalu tekan "Ctrl+Shift+R". Setelah itu boleh menyimpan ke berkas EPS.
- Template & ekstensi lainnya: https://inkscape.org/~hervyqa/

# Screenshoot

## Easy Repeat Pattern
![easy-repeat-pattern](img/ss.png)

## Easy Repeat Pattern + Mockup
![easy-repeat-pattern](img/ss-mockup.png)
